﻿'Created by Alexander Yochim

Public Class MainForm
    Private picArr(9, 9) As PictureBox
    Private gameSize As Byte = 9
    Private turn As Byte = 1 '1=Blue, 2=Red
    Private numBlueTiles As Byte 'Number of blue tiles in the GAME
    Private numRedTiles As Byte  'Number of red tiles in the GAME
    Private gameValue(1) As Short 'Slot 0 is Numerator, slot 1 is Denomenator
    Private stackValue(1) As Short 'Slot 0 is Numerator, slot 1 is Denomenator

    Private Sub MainForm_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'dynamically create (all 100) picture box game tiles
        For i As Byte = 0 To 9
            For j As Byte = 0 To 9
                picArr(i, j) = New PictureBox
                picArr(i, j).Size = New Size(32, 32)
                picArr(i, j).Margin = New Padding(1, 1, 1, 1)
                picArr(i, j).BackColor = Color.Transparent
                picArr(i, j).Location = New Point(250 + 75 * j, 418 - 34 * i)
                picArr(i, j).BringToFront()
                Controls.Add(picArr(i, j))
                AddHandler picArr(i, j).Click, AddressOf Tile_Click
            Next
        Next
    End Sub

    Private Sub btnNew_Click(sender As System.Object, e As System.EventArgs) Handles btnNew.Click
        ResetGame()
        Dim r As New Random
        Dim emptyColumn(9) As Boolean
        'Fill picture boxes for new game
        For i As Byte = 0 To gameSize
            For j As Byte = 0 To gameSize
                Dim rand As Byte
                If chkEmpty.Checked Then
                    rand = r.Next(3)     'Allows empty, blue, or red tiles
                Else
                    rand = r.Next(2) + 1 'Allows only blue or red tiles
                End If

                If (rand = 0 Or emptyColumn(j)) Then '0 = Empty Tile
                    'picArr(i, j).BackColor = Color.Green
                    picArr(i, j).Image = Nothing
                    picArr(i, j).Name = "picTile" & i & j
                    emptyColumn(j) = True
                ElseIf rand = 1 Then '1 = Blue Tile
                    picArr(i, j).Name = "blueTile" & i & j
                    picArr(i, j).Image = My.Resources.BlueTile
                    numBlueTiles += 1
                ElseIf rand = 2 Then '2 = Red Tile
                    picArr(i, j).Name = "redTile" & i & j
                    picArr(i, j).Image = My.Resources.RedTile
                    numRedTiles += 1
                End If
            Next
        Next
        stackValue(0) = 0
        stackValue(1) = 512
        CalculateSurreals(-1)

        If numBlueTiles = 0 Then
            If numRedTiles = 0 Then   'In smaller sized games, it is likely that the game will start
                btnNew.PerformClick() 'with only red piece(s) on the board, this handles that.
            Else                      'This also handles when no pieces spawn on the board
                GameOver()
            End If
        End If
    End Sub

    Private Sub Tile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim tile As PictureBox = CType(sender, System.Windows.Forms.PictureBox)
        Dim i As Byte = (Microsoft.VisualBasic.Right(tile.Name, 2) \ 10)
        Dim j As Byte = (Microsoft.VisualBasic.Right(tile.Name, 1))
        If (turn = 1) And (tile.Name.Substring(0, 1) = "b") Then
            tile.Image = Nothing
            RemoveTiles(i, j)
            turn = 2
            pnlControls.BackColor = Color.IndianRed
        ElseIf (turn = 2) And (tile.Name.Substring(0, 1) = "r") Then
            tile.Image = Nothing
            RemoveTiles(i, j)
            turn = 1
            pnlControls.BackColor = Color.CornflowerBlue
        End If
        GameOver()
    End Sub

    Private Sub RemoveTiles(i As Byte, j As Byte)
        For x As Byte = i To gameSize
            If (picArr(x, j).Name.Substring(0, 1) = "b") Then
                numBlueTiles -= 1
            ElseIf (picArr(x, j).Name.Substring(0, 1) = "r") Then
                numRedTiles -= 1
            End If
            picArr(x, j).Image = Nothing
            picArr(x, j).Name = "picTile" & x & j
        Next
        CalculateSurreals(j)
    End Sub

    Private Sub CalculateSurreals(x As SByte)
        If x = -1 Then
            For j As Byte = 0 To gameSize
                SurrealLogic(j)
            Next
        Else
            SurrealLogic(x)
        End If
        Output_G()
    End Sub

    Private Sub SurrealLogic(x As Byte)
        Dim red As Boolean = 0
        Dim blue As Boolean = 0
        Dim c As Byte = 1
        For i As Byte = 0 To gameSize
            If picArr(i, x).Name.Substring(0, 1) = "p" Then
                i = gameSize
            ElseIf picArr(i, x).Name.Substring(0, 1) = "b" Then
                If red = 0 Then
                    stackValue(0) += 512
                Else
                    stackValue(0) += 512 / (2 ^ c)
                    c += 1
                End If
                blue = True
            ElseIf picArr(i, x).Name.Substring(0, 1) = "r" Then
                If blue = 0 Then
                    stackValue(0) -= 512
                Else
                    stackValue(0) -= 512 / (2 ^ c)
                    c += 1
                End If
                red = True
            End If
        Next

        'Find greatest common denomenator and reduce the fraction
        Dim g As Short = gcd(stackValue(0), stackValue(1))
        stackValue(0) /= g
        stackValue(1) /= g

        Output_S(x)
    End Sub

    Private Sub Output_S(j As Byte)
        'Makes unused surreal value labels disappear when gamesize is decreased
        For i As Byte = (gameSize + 1) To 9
            CType(Controls("lblCenter" & i), Label).Text = Nothing
            CType(Controls("lblNum" & i), Label).Text = Nothing
            CType(Controls("lblDen" & i), Label).Text = Nothing
            CType(Controls("lblCenter" & i), Label).BackColor = Color.White
        Next

        If stackValue(0) = 0 Then
            CType(Controls("lblCenter" & j), Label).Text = 0
            CType(Controls("lblCenter" & j), Label).Location = New Point(250 + 75 * j, 78)
            CType(Controls("lblCenter" & j), Label).Size = New Size(32, 13)
            CType(Controls("lblCenter" & j), Label).BackColor = Color.White
            CType(Controls("lblNum" & j), Label).Text = Nothing
            CType(Controls("lblDen" & j), Label).Text = Nothing
        ElseIf stackValue(1) = 1 Then
            CType(Controls("lblCenter" & j), Label).Text = stackValue(0)
            CType(Controls("lblCenter" & j), Label).Location = New Point(250 + 75 * j, 78)
            CType(Controls("lblCenter" & j), Label).Size = New Size(32, 13)
            CType(Controls("lblCenter" & j), Label).BackColor = Color.White
            CType(Controls("lblNum" & j), Label).Text = Nothing
            CType(Controls("lblDen" & j), Label).Text = Nothing
        Else
            CType(Controls("lblNum" & j), Label).Text = stackValue(0)
            CType(Controls("lblDen" & j), Label).Text = stackValue(1)
            CType(Controls("lblCenter" & j), Label).Location = New Point(250 + 75 * j, 83)
            CType(Controls("lblCenter" & j), Label).Size = New Size(32, 4)
            CType(Controls("lblCenter" & j), Label).BackColor = Color.Black
            CType(Controls("lblCenter" & j), Label).Text = Nothing
        End If

        stackValue(0) = 0
        stackValue(1) = 512
    End Sub

    Private Sub Output_G()
        gameValue(0) = 0
        gameValue(1) = 512

        'Adds all the stack's numerators to the game numerator
        For j As Byte = 0 To gameSize
            If CType(Controls("lblNum" & j), Label).Text = "" Then
                gameValue(0) += (CType(Controls("lblCenter" & j), Label).Text) * 512
            Else
                gameValue(0) += (512 / (CType(Controls("lblDen" & j), Label).Text)) * (CType(Controls("lblNum" & j), Label).Text)
            End If
        Next

        'Reduces the Game Surreal Value fraction ONCE after all values are totalled
        Dim g As Short = gcd(gameValue(0), gameValue(1))
        gameValue(0) /= g
        gameValue(1) /= g

        If gameValue(0) = 0 Then
            lblGameCenter.Text = 0
            lblGameCenter.Location = New Point(130, 382)
            lblGameCenter.Size = New Size(43, 13)
            lblGameCenter.BackColor = Color.Transparent
            lblGameNum.Text = Nothing
            lblGameDen.Text = Nothing
        ElseIf gameValue(1) = 1 Then
            lblGameCenter.Text = gameValue(0)
            lblGameCenter.Location = New Point(130, 382)
            lblGameCenter.Size = New Size(43, 13)
            lblGameCenter.BackColor = Color.Transparent
            lblGameNum.Text = Nothing
            lblGameDen.Text = Nothing
        Else
            lblGameNum.Text = gameValue(0)
            lblGameDen.Text = gameValue(1)
            lblGameCenter.Location = New Point(130, 387)
            lblGameCenter.Size = New Size(43, 4)
            lblGameCenter.BackColor = Color.Black
            lblGameCenter.Text = Nothing
        End If
    End Sub

    Private Sub ResetGame()
        gameSize = udGameSize.Value - 1 'get gameSize from the Up/Down control

        pnlControls.BackColor = Color.CornflowerBlue
        turn = 1    'Blue always goes first

        numBlueTiles = 0
        numRedTiles = 0

        picWin.Image = Nothing

        'Clears remaining pieces from board for new game
        For i As Byte = 0 To 9
            For j As Byte = 0 To 9
                picArr(i, j).Image = Nothing
            Next
        Next
    End Sub

    Private Sub GameOver()
        If turn = 2 And numRedTiles = 0 Then
            picWin.Image = My.Resources.BlueWins
            pnlControls.BackColor = Color.CornflowerBlue
        ElseIf turn = 1 And numBlueTiles = 0 Then
            picWin.Image = My.Resources.RedWins
            pnlControls.BackColor = Color.IndianRed
        End If
    End Sub

    Function gcd(ByVal x As Short, ByVal y As Short) As Short
        If x Mod y = 0 Then
            If y < 0 Then
                Return -1 * y
            Else
                Return y
            End If
        Else
            Return gcd(y, x Mod y)
        End If
    End Function

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub
End Class
