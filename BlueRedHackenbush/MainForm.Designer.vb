﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblBlue = New System.Windows.Forms.Label()
        Me.lblRed = New System.Windows.Forms.Label()
        Me.lblHackenbush = New System.Windows.Forms.Label()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnNew = New System.Windows.Forms.Button()
        Me.picVBar = New System.Windows.Forms.PictureBox()
        Me.picGround = New System.Windows.Forms.PictureBox()
        Me.chkEmpty = New System.Windows.Forms.CheckBox()
        Me.udGameSize = New System.Windows.Forms.NumericUpDown()
        Me.lblGameSize = New System.Windows.Forms.Label()
        Me.lblDen0 = New System.Windows.Forms.Label()
        Me.lblDen1 = New System.Windows.Forms.Label()
        Me.lblDen2 = New System.Windows.Forms.Label()
        Me.lblDen3 = New System.Windows.Forms.Label()
        Me.lblDen5 = New System.Windows.Forms.Label()
        Me.lblDen6 = New System.Windows.Forms.Label()
        Me.lblDen7 = New System.Windows.Forms.Label()
        Me.lblDen8 = New System.Windows.Forms.Label()
        Me.lblDen9 = New System.Windows.Forms.Label()
        Me.lblNum9 = New System.Windows.Forms.Label()
        Me.lblNum8 = New System.Windows.Forms.Label()
        Me.lblNum7 = New System.Windows.Forms.Label()
        Me.lblNum6 = New System.Windows.Forms.Label()
        Me.lblNum5 = New System.Windows.Forms.Label()
        Me.lblNum4 = New System.Windows.Forms.Label()
        Me.lblNum3 = New System.Windows.Forms.Label()
        Me.lblNum2 = New System.Windows.Forms.Label()
        Me.lblNum1 = New System.Windows.Forms.Label()
        Me.lblNum0 = New System.Windows.Forms.Label()
        Me.lblGameSurrealValue = New System.Windows.Forms.Label()
        Me.lblStackSurrealValues = New System.Windows.Forms.Label()
        Me.lblDen4 = New System.Windows.Forms.Label()
        Me.pnlControls = New System.Windows.Forms.Panel()
        Me.lblGameCenter = New System.Windows.Forms.Label()
        Me.lblGameNum = New System.Windows.Forms.Label()
        Me.lblGameDen = New System.Windows.Forms.Label()
        Me.picWin = New System.Windows.Forms.PictureBox()
        Me.lblCenter9 = New System.Windows.Forms.Label()
        Me.lblCenter8 = New System.Windows.Forms.Label()
        Me.lblCenter7 = New System.Windows.Forms.Label()
        Me.lblCenter6 = New System.Windows.Forms.Label()
        Me.lblCenter5 = New System.Windows.Forms.Label()
        Me.lblCenter4 = New System.Windows.Forms.Label()
        Me.lblCenter3 = New System.Windows.Forms.Label()
        Me.lblCenter2 = New System.Windows.Forms.Label()
        Me.lblCenter1 = New System.Windows.Forms.Label()
        Me.lblCenter0 = New System.Windows.Forms.Label()
        CType(Me.picVBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picGround, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.udGameSize, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlControls.SuspendLayout()
        CType(Me.picWin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblBlue
        '
        Me.lblBlue.BackColor = System.Drawing.Color.Transparent
        Me.lblBlue.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBlue.ForeColor = System.Drawing.Color.Blue
        Me.lblBlue.Location = New System.Drawing.Point(363, 9)
        Me.lblBlue.Margin = New System.Windows.Forms.Padding(0)
        Me.lblBlue.Name = "lblBlue"
        Me.lblBlue.Size = New System.Drawing.Size(93, 41)
        Me.lblBlue.TabIndex = 100
        Me.lblBlue.Text = "Blue"
        '
        'lblRed
        '
        Me.lblRed.BackColor = System.Drawing.Color.Transparent
        Me.lblRed.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRed.ForeColor = System.Drawing.Color.Red
        Me.lblRed.Location = New System.Drawing.Point(462, 9)
        Me.lblRed.Margin = New System.Windows.Forms.Padding(0)
        Me.lblRed.Name = "lblRed"
        Me.lblRed.Size = New System.Drawing.Size(93, 41)
        Me.lblRed.TabIndex = 100
        Me.lblRed.Text = "Red"
        '
        'lblHackenbush
        '
        Me.lblHackenbush.BackColor = System.Drawing.Color.Transparent
        Me.lblHackenbush.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHackenbush.Location = New System.Drawing.Point(561, 9)
        Me.lblHackenbush.Margin = New System.Windows.Forms.Padding(0)
        Me.lblHackenbush.Name = "lblHackenbush"
        Me.lblHackenbush.Size = New System.Drawing.Size(228, 41)
        Me.lblHackenbush.TabIndex = 100
        Me.lblHackenbush.Text = "Hackenbush"
        '
        'btnExit
        '
        Me.btnExit.BackColor = System.Drawing.SystemColors.Control
        Me.btnExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(23, 194)
        Me.btnExit.Margin = New System.Windows.Forms.Padding(0)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(140, 40)
        Me.btnExit.TabIndex = 104
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = False
        '
        'btnNew
        '
        Me.btnNew.BackColor = System.Drawing.SystemColors.Control
        Me.btnNew.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.Location = New System.Drawing.Point(23, 134)
        Me.btnNew.Margin = New System.Windows.Forms.Padding(0)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(140, 40)
        Me.btnNew.TabIndex = 103
        Me.btnNew.Text = "New Game"
        Me.btnNew.UseVisualStyleBackColor = False
        '
        'picVBar
        '
        Me.picVBar.BackColor = System.Drawing.Color.Black
        Me.picVBar.ErrorImage = Nothing
        Me.picVBar.InitialImage = Nothing
        Me.picVBar.Location = New System.Drawing.Point(182, 0)
        Me.picVBar.Margin = New System.Windows.Forms.Padding(0)
        Me.picVBar.Name = "picVBar"
        Me.picVBar.Size = New System.Drawing.Size(5, 462)
        Me.picVBar.TabIndex = 106
        Me.picVBar.TabStop = False
        '
        'picGround
        '
        Me.picGround.BackColor = System.Drawing.Color.Black
        Me.picGround.ErrorImage = Nothing
        Me.picGround.InitialImage = Nothing
        Me.picGround.Location = New System.Drawing.Point(187, 450)
        Me.picGround.Margin = New System.Windows.Forms.Padding(0)
        Me.picGround.Name = "picGround"
        Me.picGround.Size = New System.Drawing.Size(797, 12)
        Me.picGround.TabIndex = 108
        Me.picGround.TabStop = False
        '
        'chkEmpty
        '
        Me.chkEmpty.AutoSize = True
        Me.chkEmpty.BackColor = System.Drawing.Color.Transparent
        Me.chkEmpty.Checked = True
        Me.chkEmpty.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkEmpty.Location = New System.Drawing.Point(23, 263)
        Me.chkEmpty.Margin = New System.Windows.Forms.Padding(0)
        Me.chkEmpty.Name = "chkEmpty"
        Me.chkEmpty.Size = New System.Drawing.Size(124, 17)
        Me.chkEmpty.TabIndex = 109
        Me.chkEmpty.Text = "Allow Empty Blocks?"
        Me.chkEmpty.UseVisualStyleBackColor = False
        '
        'udGameSize
        '
        Me.udGameSize.BackColor = System.Drawing.SystemColors.Control
        Me.udGameSize.Location = New System.Drawing.Point(114, 303)
        Me.udGameSize.Margin = New System.Windows.Forms.Padding(0)
        Me.udGameSize.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.udGameSize.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.udGameSize.Name = "udGameSize"
        Me.udGameSize.Size = New System.Drawing.Size(39, 20)
        Me.udGameSize.TabIndex = 111
        Me.udGameSize.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        'lblGameSize
        '
        Me.lblGameSize.AutoSize = True
        Me.lblGameSize.BackColor = System.Drawing.Color.Transparent
        Me.lblGameSize.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGameSize.Location = New System.Drawing.Point(20, 303)
        Me.lblGameSize.Margin = New System.Windows.Forms.Padding(0)
        Me.lblGameSize.Name = "lblGameSize"
        Me.lblGameSize.Size = New System.Drawing.Size(77, 16)
        Me.lblGameSize.TabIndex = 112
        Me.lblGameSize.Text = "Game Size:"
        '
        'lblDen0
        '
        Me.lblDen0.BackColor = System.Drawing.Color.Transparent
        Me.lblDen0.Location = New System.Drawing.Point(250, 90)
        Me.lblDen0.Margin = New System.Windows.Forms.Padding(0)
        Me.lblDen0.Name = "lblDen0"
        Me.lblDen0.Size = New System.Drawing.Size(32, 13)
        Me.lblDen0.TabIndex = 113
        Me.lblDen0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDen1
        '
        Me.lblDen1.BackColor = System.Drawing.Color.Transparent
        Me.lblDen1.Location = New System.Drawing.Point(325, 90)
        Me.lblDen1.Margin = New System.Windows.Forms.Padding(0)
        Me.lblDen1.Name = "lblDen1"
        Me.lblDen1.Size = New System.Drawing.Size(32, 13)
        Me.lblDen1.TabIndex = 114
        Me.lblDen1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDen2
        '
        Me.lblDen2.BackColor = System.Drawing.Color.Transparent
        Me.lblDen2.Location = New System.Drawing.Point(400, 90)
        Me.lblDen2.Margin = New System.Windows.Forms.Padding(0)
        Me.lblDen2.Name = "lblDen2"
        Me.lblDen2.Size = New System.Drawing.Size(32, 13)
        Me.lblDen2.TabIndex = 115
        Me.lblDen2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDen3
        '
        Me.lblDen3.BackColor = System.Drawing.Color.Transparent
        Me.lblDen3.Location = New System.Drawing.Point(475, 90)
        Me.lblDen3.Margin = New System.Windows.Forms.Padding(0)
        Me.lblDen3.Name = "lblDen3"
        Me.lblDen3.Size = New System.Drawing.Size(32, 13)
        Me.lblDen3.TabIndex = 116
        Me.lblDen3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDen5
        '
        Me.lblDen5.BackColor = System.Drawing.Color.Transparent
        Me.lblDen5.Location = New System.Drawing.Point(625, 90)
        Me.lblDen5.Margin = New System.Windows.Forms.Padding(0)
        Me.lblDen5.Name = "lblDen5"
        Me.lblDen5.Size = New System.Drawing.Size(32, 13)
        Me.lblDen5.TabIndex = 118
        Me.lblDen5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDen6
        '
        Me.lblDen6.BackColor = System.Drawing.Color.Transparent
        Me.lblDen6.Location = New System.Drawing.Point(700, 90)
        Me.lblDen6.Margin = New System.Windows.Forms.Padding(0)
        Me.lblDen6.Name = "lblDen6"
        Me.lblDen6.Size = New System.Drawing.Size(32, 13)
        Me.lblDen6.TabIndex = 119
        Me.lblDen6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDen7
        '
        Me.lblDen7.BackColor = System.Drawing.Color.Transparent
        Me.lblDen7.Location = New System.Drawing.Point(775, 90)
        Me.lblDen7.Margin = New System.Windows.Forms.Padding(0)
        Me.lblDen7.Name = "lblDen7"
        Me.lblDen7.Size = New System.Drawing.Size(32, 13)
        Me.lblDen7.TabIndex = 120
        Me.lblDen7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDen8
        '
        Me.lblDen8.BackColor = System.Drawing.Color.Transparent
        Me.lblDen8.Location = New System.Drawing.Point(850, 90)
        Me.lblDen8.Margin = New System.Windows.Forms.Padding(0)
        Me.lblDen8.Name = "lblDen8"
        Me.lblDen8.Size = New System.Drawing.Size(32, 13)
        Me.lblDen8.TabIndex = 121
        Me.lblDen8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDen9
        '
        Me.lblDen9.BackColor = System.Drawing.Color.Transparent
        Me.lblDen9.Location = New System.Drawing.Point(925, 90)
        Me.lblDen9.Margin = New System.Windows.Forms.Padding(0)
        Me.lblDen9.Name = "lblDen9"
        Me.lblDen9.Size = New System.Drawing.Size(32, 13)
        Me.lblDen9.TabIndex = 122
        Me.lblDen9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNum9
        '
        Me.lblNum9.BackColor = System.Drawing.Color.Transparent
        Me.lblNum9.Location = New System.Drawing.Point(925, 67)
        Me.lblNum9.Margin = New System.Windows.Forms.Padding(0)
        Me.lblNum9.Name = "lblNum9"
        Me.lblNum9.Size = New System.Drawing.Size(32, 13)
        Me.lblNum9.TabIndex = 132
        Me.lblNum9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNum8
        '
        Me.lblNum8.BackColor = System.Drawing.Color.Transparent
        Me.lblNum8.Location = New System.Drawing.Point(850, 67)
        Me.lblNum8.Margin = New System.Windows.Forms.Padding(0)
        Me.lblNum8.Name = "lblNum8"
        Me.lblNum8.Size = New System.Drawing.Size(32, 13)
        Me.lblNum8.TabIndex = 131
        Me.lblNum8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNum7
        '
        Me.lblNum7.BackColor = System.Drawing.Color.Transparent
        Me.lblNum7.Location = New System.Drawing.Point(775, 67)
        Me.lblNum7.Margin = New System.Windows.Forms.Padding(0)
        Me.lblNum7.Name = "lblNum7"
        Me.lblNum7.Size = New System.Drawing.Size(32, 13)
        Me.lblNum7.TabIndex = 130
        Me.lblNum7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNum6
        '
        Me.lblNum6.BackColor = System.Drawing.Color.Transparent
        Me.lblNum6.Location = New System.Drawing.Point(700, 67)
        Me.lblNum6.Margin = New System.Windows.Forms.Padding(0)
        Me.lblNum6.Name = "lblNum6"
        Me.lblNum6.Size = New System.Drawing.Size(32, 13)
        Me.lblNum6.TabIndex = 129
        Me.lblNum6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNum5
        '
        Me.lblNum5.BackColor = System.Drawing.Color.Transparent
        Me.lblNum5.Location = New System.Drawing.Point(625, 67)
        Me.lblNum5.Margin = New System.Windows.Forms.Padding(0)
        Me.lblNum5.Name = "lblNum5"
        Me.lblNum5.Size = New System.Drawing.Size(32, 13)
        Me.lblNum5.TabIndex = 128
        Me.lblNum5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNum4
        '
        Me.lblNum4.BackColor = System.Drawing.Color.Transparent
        Me.lblNum4.Location = New System.Drawing.Point(550, 67)
        Me.lblNum4.Margin = New System.Windows.Forms.Padding(0)
        Me.lblNum4.Name = "lblNum4"
        Me.lblNum4.Size = New System.Drawing.Size(32, 13)
        Me.lblNum4.TabIndex = 127
        Me.lblNum4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNum3
        '
        Me.lblNum3.BackColor = System.Drawing.Color.Transparent
        Me.lblNum3.Location = New System.Drawing.Point(475, 67)
        Me.lblNum3.Margin = New System.Windows.Forms.Padding(0)
        Me.lblNum3.Name = "lblNum3"
        Me.lblNum3.Size = New System.Drawing.Size(32, 13)
        Me.lblNum3.TabIndex = 126
        Me.lblNum3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNum2
        '
        Me.lblNum2.BackColor = System.Drawing.Color.Transparent
        Me.lblNum2.Location = New System.Drawing.Point(400, 67)
        Me.lblNum2.Margin = New System.Windows.Forms.Padding(0)
        Me.lblNum2.Name = "lblNum2"
        Me.lblNum2.Size = New System.Drawing.Size(32, 13)
        Me.lblNum2.TabIndex = 125
        Me.lblNum2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNum1
        '
        Me.lblNum1.BackColor = System.Drawing.Color.Transparent
        Me.lblNum1.Location = New System.Drawing.Point(325, 67)
        Me.lblNum1.Margin = New System.Windows.Forms.Padding(0)
        Me.lblNum1.Name = "lblNum1"
        Me.lblNum1.Size = New System.Drawing.Size(32, 13)
        Me.lblNum1.TabIndex = 124
        Me.lblNum1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNum0
        '
        Me.lblNum0.BackColor = System.Drawing.Color.Transparent
        Me.lblNum0.Location = New System.Drawing.Point(250, 67)
        Me.lblNum0.Margin = New System.Windows.Forms.Padding(0)
        Me.lblNum0.Name = "lblNum0"
        Me.lblNum0.Size = New System.Drawing.Size(32, 13)
        Me.lblNum0.TabIndex = 123
        Me.lblNum0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblGameSurrealValue
        '
        Me.lblGameSurrealValue.AutoSize = True
        Me.lblGameSurrealValue.BackColor = System.Drawing.Color.Transparent
        Me.lblGameSurrealValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGameSurrealValue.Location = New System.Drawing.Point(1, 381)
        Me.lblGameSurrealValue.Margin = New System.Windows.Forms.Padding(0)
        Me.lblGameSurrealValue.Name = "lblGameSurrealValue"
        Me.lblGameSurrealValue.Size = New System.Drawing.Size(123, 13)
        Me.lblGameSurrealValue.TabIndex = 139
        Me.lblGameSurrealValue.Text = "Game Surreal Value:"
        '
        'lblStackSurrealValues
        '
        Me.lblStackSurrealValues.BackColor = System.Drawing.Color.Transparent
        Me.lblStackSurrealValues.Location = New System.Drawing.Point(194, 66)
        Me.lblStackSurrealValues.Margin = New System.Windows.Forms.Padding(0)
        Me.lblStackSurrealValues.Name = "lblStackSurrealValues"
        Me.lblStackSurrealValues.Size = New System.Drawing.Size(43, 39)
        Me.lblStackSurrealValues.TabIndex = 140
        Me.lblStackSurrealValues.Text = "Stack Surreal: Values"
        '
        'lblDen4
        '
        Me.lblDen4.BackColor = System.Drawing.Color.Transparent
        Me.lblDen4.Location = New System.Drawing.Point(550, 90)
        Me.lblDen4.Margin = New System.Windows.Forms.Padding(0)
        Me.lblDen4.Name = "lblDen4"
        Me.lblDen4.Size = New System.Drawing.Size(32, 13)
        Me.lblDen4.TabIndex = 117
        Me.lblDen4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlControls
        '
        Me.pnlControls.Controls.Add(Me.lblGameCenter)
        Me.pnlControls.Controls.Add(Me.lblGameNum)
        Me.pnlControls.Controls.Add(Me.lblGameDen)
        Me.pnlControls.Controls.Add(Me.picWin)
        Me.pnlControls.Controls.Add(Me.btnNew)
        Me.pnlControls.Controls.Add(Me.btnExit)
        Me.pnlControls.Controls.Add(Me.chkEmpty)
        Me.pnlControls.Controls.Add(Me.udGameSize)
        Me.pnlControls.Controls.Add(Me.lblGameSurrealValue)
        Me.pnlControls.Controls.Add(Me.lblGameSize)
        Me.pnlControls.Dock = System.Windows.Forms.DockStyle.Left
        Me.pnlControls.Location = New System.Drawing.Point(0, 0)
        Me.pnlControls.Margin = New System.Windows.Forms.Padding(0)
        Me.pnlControls.Name = "pnlControls"
        Me.pnlControls.Size = New System.Drawing.Size(184, 462)
        Me.pnlControls.TabIndex = 144
        '
        'lblGameCenter
        '
        Me.lblGameCenter.BackColor = System.Drawing.Color.Black
        Me.lblGameCenter.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGameCenter.Location = New System.Drawing.Point(130, 387)
        Me.lblGameCenter.Margin = New System.Windows.Forms.Padding(0)
        Me.lblGameCenter.Name = "lblGameCenter"
        Me.lblGameCenter.Size = New System.Drawing.Size(43, 4)
        Me.lblGameCenter.TabIndex = 151
        Me.lblGameCenter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblGameNum
        '
        Me.lblGameNum.BackColor = System.Drawing.Color.Transparent
        Me.lblGameNum.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGameNum.Location = New System.Drawing.Point(130, 371)
        Me.lblGameNum.Margin = New System.Windows.Forms.Padding(0)
        Me.lblGameNum.Name = "lblGameNum"
        Me.lblGameNum.Size = New System.Drawing.Size(43, 13)
        Me.lblGameNum.TabIndex = 150
        Me.lblGameNum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblGameDen
        '
        Me.lblGameDen.BackColor = System.Drawing.Color.Transparent
        Me.lblGameDen.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGameDen.Location = New System.Drawing.Point(130, 394)
        Me.lblGameDen.Margin = New System.Windows.Forms.Padding(0)
        Me.lblGameDen.Name = "lblGameDen"
        Me.lblGameDen.Size = New System.Drawing.Size(43, 13)
        Me.lblGameDen.TabIndex = 149
        Me.lblGameDen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'picWin
        '
        Me.picWin.BackColor = System.Drawing.Color.Transparent
        Me.picWin.Location = New System.Drawing.Point(0, 0)
        Me.picWin.Margin = New System.Windows.Forms.Padding(0)
        Me.picWin.Name = "picWin"
        Me.picWin.Size = New System.Drawing.Size(184, 103)
        Me.picWin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picWin.TabIndex = 140
        Me.picWin.TabStop = False
        '
        'lblCenter9
        '
        Me.lblCenter9.BackColor = System.Drawing.Color.Black
        Me.lblCenter9.Location = New System.Drawing.Point(925, 83)
        Me.lblCenter9.Margin = New System.Windows.Forms.Padding(0)
        Me.lblCenter9.Name = "lblCenter9"
        Me.lblCenter9.Size = New System.Drawing.Size(32, 4)
        Me.lblCenter9.TabIndex = 154
        Me.lblCenter9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCenter8
        '
        Me.lblCenter8.BackColor = System.Drawing.Color.Black
        Me.lblCenter8.Location = New System.Drawing.Point(850, 83)
        Me.lblCenter8.Margin = New System.Windows.Forms.Padding(0)
        Me.lblCenter8.Name = "lblCenter8"
        Me.lblCenter8.Size = New System.Drawing.Size(32, 4)
        Me.lblCenter8.TabIndex = 153
        Me.lblCenter8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCenter7
        '
        Me.lblCenter7.BackColor = System.Drawing.Color.Black
        Me.lblCenter7.Location = New System.Drawing.Point(775, 83)
        Me.lblCenter7.Margin = New System.Windows.Forms.Padding(0)
        Me.lblCenter7.Name = "lblCenter7"
        Me.lblCenter7.Size = New System.Drawing.Size(32, 4)
        Me.lblCenter7.TabIndex = 152
        Me.lblCenter7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCenter6
        '
        Me.lblCenter6.BackColor = System.Drawing.Color.Black
        Me.lblCenter6.Location = New System.Drawing.Point(700, 83)
        Me.lblCenter6.Margin = New System.Windows.Forms.Padding(0)
        Me.lblCenter6.Name = "lblCenter6"
        Me.lblCenter6.Size = New System.Drawing.Size(32, 4)
        Me.lblCenter6.TabIndex = 151
        Me.lblCenter6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCenter5
        '
        Me.lblCenter5.BackColor = System.Drawing.Color.Black
        Me.lblCenter5.Location = New System.Drawing.Point(625, 83)
        Me.lblCenter5.Margin = New System.Windows.Forms.Padding(0)
        Me.lblCenter5.Name = "lblCenter5"
        Me.lblCenter5.Size = New System.Drawing.Size(32, 4)
        Me.lblCenter5.TabIndex = 150
        Me.lblCenter5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCenter4
        '
        Me.lblCenter4.BackColor = System.Drawing.Color.Black
        Me.lblCenter4.Location = New System.Drawing.Point(550, 83)
        Me.lblCenter4.Margin = New System.Windows.Forms.Padding(0)
        Me.lblCenter4.Name = "lblCenter4"
        Me.lblCenter4.Size = New System.Drawing.Size(32, 4)
        Me.lblCenter4.TabIndex = 149
        Me.lblCenter4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCenter3
        '
        Me.lblCenter3.BackColor = System.Drawing.Color.Black
        Me.lblCenter3.Location = New System.Drawing.Point(475, 83)
        Me.lblCenter3.Margin = New System.Windows.Forms.Padding(0)
        Me.lblCenter3.Name = "lblCenter3"
        Me.lblCenter3.Size = New System.Drawing.Size(32, 4)
        Me.lblCenter3.TabIndex = 148
        Me.lblCenter3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCenter2
        '
        Me.lblCenter2.BackColor = System.Drawing.Color.Black
        Me.lblCenter2.Location = New System.Drawing.Point(400, 83)
        Me.lblCenter2.Margin = New System.Windows.Forms.Padding(0)
        Me.lblCenter2.Name = "lblCenter2"
        Me.lblCenter2.Size = New System.Drawing.Size(32, 4)
        Me.lblCenter2.TabIndex = 147
        Me.lblCenter2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCenter1
        '
        Me.lblCenter1.BackColor = System.Drawing.Color.Black
        Me.lblCenter1.Location = New System.Drawing.Point(325, 83)
        Me.lblCenter1.Margin = New System.Windows.Forms.Padding(0)
        Me.lblCenter1.Name = "lblCenter1"
        Me.lblCenter1.Size = New System.Drawing.Size(32, 4)
        Me.lblCenter1.TabIndex = 146
        Me.lblCenter1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCenter0
        '
        Me.lblCenter0.BackColor = System.Drawing.Color.Black
        Me.lblCenter0.Location = New System.Drawing.Point(250, 83)
        Me.lblCenter0.Margin = New System.Windows.Forms.Padding(0)
        Me.lblCenter0.Name = "lblCenter0"
        Me.lblCenter0.Size = New System.Drawing.Size(32, 4)
        Me.lblCenter0.TabIndex = 145
        Me.lblCenter0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(984, 462)
        Me.Controls.Add(Me.pnlControls)
        Me.Controls.Add(Me.lblStackSurrealValues)
        Me.Controls.Add(Me.picGround)
        Me.Controls.Add(Me.picVBar)
        Me.Controls.Add(Me.lblHackenbush)
        Me.Controls.Add(Me.lblRed)
        Me.Controls.Add(Me.lblBlue)
        Me.Controls.Add(Me.lblCenter0)
        Me.Controls.Add(Me.lblNum9)
        Me.Controls.Add(Me.lblNum8)
        Me.Controls.Add(Me.lblNum7)
        Me.Controls.Add(Me.lblNum6)
        Me.Controls.Add(Me.lblNum5)
        Me.Controls.Add(Me.lblNum4)
        Me.Controls.Add(Me.lblNum3)
        Me.Controls.Add(Me.lblNum2)
        Me.Controls.Add(Me.lblNum1)
        Me.Controls.Add(Me.lblNum0)
        Me.Controls.Add(Me.lblDen9)
        Me.Controls.Add(Me.lblDen8)
        Me.Controls.Add(Me.lblDen7)
        Me.Controls.Add(Me.lblDen6)
        Me.Controls.Add(Me.lblDen5)
        Me.Controls.Add(Me.lblDen4)
        Me.Controls.Add(Me.lblDen3)
        Me.Controls.Add(Me.lblDen2)
        Me.Controls.Add(Me.lblDen1)
        Me.Controls.Add(Me.lblDen0)
        Me.Controls.Add(Me.lblCenter9)
        Me.Controls.Add(Me.lblCenter8)
        Me.Controls.Add(Me.lblCenter7)
        Me.Controls.Add(Me.lblCenter6)
        Me.Controls.Add(Me.lblCenter5)
        Me.Controls.Add(Me.lblCenter4)
        Me.Controls.Add(Me.lblCenter3)
        Me.Controls.Add(Me.lblCenter2)
        Me.Controls.Add(Me.lblCenter1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.picVBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picGround, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.udGameSize, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlControls.ResumeLayout(False)
        Me.pnlControls.PerformLayout()
        CType(Me.picWin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblBlue As System.Windows.Forms.Label
    Friend WithEvents lblRed As System.Windows.Forms.Label
    Friend WithEvents lblHackenbush As System.Windows.Forms.Label
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents picVBar As System.Windows.Forms.PictureBox
    Friend WithEvents picGround As System.Windows.Forms.PictureBox
    Friend WithEvents chkEmpty As System.Windows.Forms.CheckBox
    Friend WithEvents udGameSize As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblGameSize As System.Windows.Forms.Label
    Friend WithEvents lblDen0 As System.Windows.Forms.Label
    Friend WithEvents lblDen1 As System.Windows.Forms.Label
    Friend WithEvents lblDen2 As System.Windows.Forms.Label
    Friend WithEvents lblDen3 As System.Windows.Forms.Label
    Friend WithEvents lblDen5 As System.Windows.Forms.Label
    Friend WithEvents lblDen6 As System.Windows.Forms.Label
    Friend WithEvents lblDen7 As System.Windows.Forms.Label
    Friend WithEvents lblDen8 As System.Windows.Forms.Label
    Friend WithEvents lblDen9 As System.Windows.Forms.Label
    Friend WithEvents lblNum9 As System.Windows.Forms.Label
    Friend WithEvents lblNum8 As System.Windows.Forms.Label
    Friend WithEvents lblNum7 As System.Windows.Forms.Label
    Friend WithEvents lblNum6 As System.Windows.Forms.Label
    Friend WithEvents lblNum5 As System.Windows.Forms.Label
    Friend WithEvents lblNum4 As System.Windows.Forms.Label
    Friend WithEvents lblNum3 As System.Windows.Forms.Label
    Friend WithEvents lblNum2 As System.Windows.Forms.Label
    Friend WithEvents lblNum1 As System.Windows.Forms.Label
    Friend WithEvents lblNum0 As System.Windows.Forms.Label
    Friend WithEvents lblGameSurrealValue As System.Windows.Forms.Label
    Friend WithEvents lblStackSurrealValues As System.Windows.Forms.Label
    Friend WithEvents lblDen4 As System.Windows.Forms.Label
    Friend WithEvents pnlControls As System.Windows.Forms.Panel
    Friend WithEvents lblCenter9 As System.Windows.Forms.Label
    Friend WithEvents lblCenter8 As System.Windows.Forms.Label
    Friend WithEvents lblCenter7 As System.Windows.Forms.Label
    Friend WithEvents lblCenter6 As System.Windows.Forms.Label
    Friend WithEvents lblCenter5 As System.Windows.Forms.Label
    Friend WithEvents lblCenter4 As System.Windows.Forms.Label
    Friend WithEvents lblCenter3 As System.Windows.Forms.Label
    Friend WithEvents lblCenter2 As System.Windows.Forms.Label
    Friend WithEvents lblCenter1 As System.Windows.Forms.Label
    Friend WithEvents lblCenter0 As System.Windows.Forms.Label
    Friend WithEvents picWin As System.Windows.Forms.PictureBox
    Friend WithEvents lblGameCenter As System.Windows.Forms.Label
    Friend WithEvents lblGameNum As System.Windows.Forms.Label
    Friend WithEvents lblGameDen As System.Windows.Forms.Label

End Class
