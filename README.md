BlueRedHackenbush
=================
(Code and design by Alexander Yochim)

Blue-Red Hackenbush is a two-player impartial game commonly used to demonstrate the concepts of game theory. The game board consists of stacks (either random or created) of game pieces (either blue or red) laid on a "ground" which is usually just a line. On each player's turn, they must remove a piece of their own color. If they have no pieces left to remove, they lose. When a piece is removed, all pieces above that piece are also removed. The strategy is to both remove as many of your opponents pieces as possible while also protecting your own (by taking them before your opponent does).

My program randomly generates a square game board of user-chosen size (1x1 - 10x10), and consistently analyzes the game theory simplified values of each stack and the entire game. These values determine which player will win (with optimal moves made).
